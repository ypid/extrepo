package Debian::ExtRepo::Commands::Update;

use Debian::ExtRepo::Commands::Enable;

sub run {
	my $repo = shift;
	die "Update without a repository is not yet supported\n" unless length($repo) > 0;
	return Debian::ExtRepo::Commands::Enable::run($repo, 1);
}

1;
