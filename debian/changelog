extrepo (0.8) unstable; urgency=medium

  * Fix grep syntax so that policy filtering on repository-wide policies
    actually works.

 -- Wouter Verhelst <wouter@debian.org>  Mon, 25 May 2020 10:00:46 +0200

extrepo (0.7) unstable; urgency=medium

  * Explicitly set the umask. Closes: #949956.
  * Improve error messages, documentation, and command output. Closes: #947067.

 -- Wouter Verhelst <wouter@debian.org>  Mon, 20 Apr 2020 15:41:21 +0200

extrepo (0.6) unstable; urgency=medium

  * Rebuild with changes listed in previous upload actually working. When you
    build & test on two machines, it's important to keep your git checkouts in
    sync...

 -- Wouter Verhelst <wouter@debian.org>  Wed, 04 Dec 2019 11:39:46 +0200

extrepo (0.5) unstable; urgency=medium

  * debian/tests/notfound: add
  * ExtRepo::Data: use the LoadFoile method from YAML::XS, rather than
    Load, as the latter seems to sometimes be problematic (and we need
    to dump YAML data to file for gpg validation anyway)
  * ExtRepo::Data: parse gpgv's --status-fd output rather than relying
    on exit state, which is not reliable.
  * extrepo: add "use utf8" to be somewhat more unicode-clean
  * ExtRepo::Commands::Enable: Fix parsing of enabled policies. While at
    it, replace map by a for loop, which seems easier to understand.

 -- Wouter Verhelst <wouter@debian.org>  Wed, 04 Dec 2019 11:34:32 +0200

extrepo (0.4) unstable; urgency=medium

  * debian/control: depend on libdpkg-perl, too
  * debian/control, lib/Debian/ExtRepo/Data.pm,
    lib/Debian/ExtRepo/Commands/Search.pm: change from YAML to
    YAML::XS. The latter is more complete, and is used by the process
    scripts, too; the YAML module can't grok all of the output of the
    YAML::XS module, apparently.
  * debian/tests/*: add test suite

 -- Wouter Verhelst <wouter@debian.org>  Wed, 20 Nov 2019 14:56:28 +0200

extrepo (0.3) unstable; urgency=medium

  * Add man page for extrepo
  * Fix bug in extrepo search introduced after change of metadata format.
  * Lintian fixes to package description and debian/copyright
  * debian/source/format: Mark as 3.0 (native) package
  * debian/control: add dependency on libcryptx-perl. Closes: #945047.
  * Sync config directory in binary with installed directory from packaging.
    Closes: #945053.
  * Rework comments in config.yaml slightly. Closes: #945054
  * Ship config for bullseye, not buster.

 -- Wouter Verhelst <wouter@debian.org>  Tue, 19 Nov 2019 09:31:46 +0200

extrepo (0.2) unstable; urgency=medium

  * Bump Standards-Level and Debhelper Compat level.
  * Rebuild with binary. Apparently uploads to NEW require them...

 -- Wouter Verhelst <wouter@debian.org>  Sun, 10 Nov 2019 12:50:00 +0200

extrepo (0.1) unstable; urgency=medium

  * Initial release.

 -- Wouter Verhelst <wouter@debian.org>  Wed, 23 Oct 2019 15:35:37 +0200
